<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Google Cloud Messaging Keys
    |--------------------------------------------------------------------------
    |
    | Here you may set your Server API Key in order to enable the
    | package to send notification messages to client apps.
    |
    */

    'server_api_key'	=> 'YOUR_SERVER_API_KEY',
];