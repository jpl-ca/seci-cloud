<?php

namespace GoogleCloudMessaging;

use \Curl\Curl;
use GoogleCloudMessaging\Exception\GoogleCloudMessagingException;

class GCM
{
	protected $server_api_key;
	protected $curl;
	const GOOGLE_CLOUD_MESSAGING_API_URL = 'https://gcm-http.googleapis.com/gcm/send';

	public function __construct($server_api_key)
	{
		$this->server_api_key = $server_api_key;
	}

	public function getServerApiKey()
	{
		return $this->server_api_key;
	}

	protected function validateSendableParameters($sendableParameters)
	{
		$validatedSendableParameters = array();

		$validatedSendableParameters['data'] = isset($sendableParameters['data']) ? $sendableParameters['data'] : null;
		$validatedSendableParameters['notification'] = isset($sendableParameters['notification']) ? $sendableParameters['notification'] : null;
		
		if(isset($sendableParameters['to']))
		{
			if(is_array($sendableParameters['to']))
			{
				$validatedSendableParameters['registration_ids'] = $sendableParameters['to'];
			}else{
				$validatedSendableParameters['to'] = $sendableParameters['to'];
			}
		}else{
			$validatedSendableParameters['to'] = null;
		}

		return $validatedSendableParameters;
	}

	public function send(array $sendableParameters)
	{
		$validatedSendableParameters = $this->validateSendableParameters($sendableParameters);
		$curl = new Curl(self::GOOGLE_CLOUD_MESSAGING_API_URL);
		$curl->setHeader("Content-Type", "application/json");
		$curl->setHeader("Authorization", "key=$this->server_api_key");
		$curl->post($validatedSendableParameters);
		if ($curl->error) {
			throw new GoogleCloudMessagingException($curl->errorMessage, $curl->errorCode);
			exit();
		}
		else {
			$curl->close();
			return $curl->response;
		}
	}
}