<?php

namespace GoogleCloudMessaging\Facades;

use Illuminate\Support\Facades\Facade;

class GCM extends Facade
{
    protected static function getFacadeAccessor()
    {
    	return 'GoogleCloudMessaging\GCM';
    }
}