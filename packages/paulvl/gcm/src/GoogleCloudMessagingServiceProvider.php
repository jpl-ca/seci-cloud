<?php

namespace GoogleCloudMessaging;

use Illuminate\Support\ServiceProvider;
use GoogleCloudMessaging\GCM;

class GoogleCloudMessagingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/../config/gcm.php' => config_path('gcm.php')], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GoogleCloudMessaging\GCM', function ($app) {
            return new GCM($app['config']['gcm.server_api_key']);
        });
    }    

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['GoogleCloudMessaging\GCM'];
    }
}
