<?php

namespace GoogleCloudMessaging\Exception;

use Exception;

class GoogleCloudMessagingException extends Exception
{
	// Redefine the exception so message isn't optional
	public function __construct($message, $code = 0, Exception $previous = null) {
		// some code
		//$message = 'Google Server response: "Error '.$code.' '.$message.'"';
		// make sure everything is assigned properly
		parent::__construct($message, $code, $previous);
	}

	// custom string representation of object
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}	
}