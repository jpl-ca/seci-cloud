#GCM

## **Introduction**

GCM is a library that make using Google Cloud Messaging an easy job.

## **Quick Installation**

Begin by installing this package through Composer.

You can run:

    composer require paulvl/gcm 1.*

Or edit your project's composer.json file to require paulvl/backup.
```
    "require": {
        "paulvl/gcm": "1.*"
    }
```
Next, update Composer from the Terminal:

    composer update

Once the package's installation completes, you can start using it

##**Methods**

###Send Method

The `send` method will allow you to send a notification to one reciver or more at the same time.
The `send` method accepts an `array` as argument that **must have at least two** elements:
- First and **mandatory**, an element identified with **to** as key name.
- The second element **should** be identified as **data** or **notification** as key name, but you can send **both**.

Element     | Type
:- | :-:
to | string / array
data | string / array
notification | string / array

##**Examples**

###Sending a notification

```
$gcm = new \GoogleCloudMessaging\GCM('YOUR_SERVER_API_KEY');
$serverResponse = $gcm->send([
	'to' => 'SOME_RECIVER_GCM_ID',
	'data' => ['field' => 'information'],
	'notification' => ['message' => 'this is a notification']
]);

```
###Sending a notification to Multiple Recivers

```
$gcm = new \GoogleCloudMessaging\GCM('YOUR_SERVER_API_KEY');
$serverResponse = $gcm->send([
	'to' => ['RECIVER_A_GCM_ID', 'RECIVER_B_GCM_ID'],
	'data' => ['field' => 'information'],
	'notification' => ['message' => 'this is a notification']
]);

```

## **Contribute and share ;-)**
If you like this little piece of code share it with you friends and feel free to contribute with any improvements.

