#SECI Cloud

##Installation

Begin by installing the project dependencies:

	composer install

then create an .env file:

	cp .env.example .env

Create and application key:

	php artisan key:generate

Make changes on your .env files if necesary, the cache the configuration file:

	php artisan config:cache

Do not forget to change storage folder permisson to 777 :

	chmod -R 777 storage/

And finally dump composer autoload:

	composer dump-autoload

###Supervisord

First we need to install Setuptools:

	sudo python /path/to/project/install/setuptools-18.5/setup.py install

Then install supervisord using easy_install:

	sudo easy_install supervisor

Next, you have to properly change paths in /path/to/project/supervisord.conf file, the you have to copy that file to /usr/local/etc/supervisord.conf, /usr/local/supervisord.conf, supervisord.conf or /etc/supervisord.conf

	sudo cp supervisord.conf /usr/local/etc/supervisord.conf

then start the service:

	supervisord

Do not forget to set queue as database on .env file, when is changed, recache configurations:

	php artisan config:cache

Finally reload configuration file and start the worker:

	supervisorctl reload
	supervisorctl reread
	supervisorctl update
	supervisorctl start seci-cloud-worker:*
