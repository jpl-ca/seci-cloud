<?php

namespace Seci\Libraries;

use Location\Coordinate;
use Location\Distance\Vincenty;

class Geo
{
	public static function createCoordinate($lat, $lng)
	{
		if(is_null($lat) || is_null($lng))
		{
			throw new Exception('Latitude or longitude can not be null.');
		}
		return new Coordinate($lat, $lng);
	}

	public static function calculateDistance($latA, $lngA, $latB = null, $lngB = null)
	{
		if(!$latA instanceof Coordinate && !$lngA instanceof Coordinate)
		{
			$pointA = new Coordinate($latA, $lngA);
			$pointB = new Coordinate($latB, $lngB);
		}else{
			$pointA = $latA;
			$pointB = $lngA;
		}
		return $pointA->getDistance($pointB, new Vincenty()); // returns distance in meters
	}

	public static function coordinateInRadius(Coordinate $radiusCenter, $radius, Coordinate $coordinate)
	{
		return self::calculateDistance($radiusCenter, $coordinate) <= $radius;
	}
}