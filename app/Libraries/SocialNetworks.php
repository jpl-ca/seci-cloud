<?php

namespace Seci\Libraries;

use Seci\Libraries\Facebook;
use Seci\Libraries\Google;
use Seci\Models\User;
use Seci\Models\SocialAccount;

class SocialNetworks
{
	const FACEBOOK = 'facebook';
	const GOOGLE = 'google';
	public static $REGISTERED_SOCIAL_NETWORKS = ['facebook','google'];
	const INVALID_TOKEN_ERROR = 'invalid_token_error';
	const USER_EMAIL_NOT_REGISTERED = 'user_email_not_registered';

	public static function getUser($socialNetworkIdentifier, $accessToken)
	{
		$user = null;

		switch ($socialNetworkIdentifier) {
			case self::FACEBOOK:
				$facebookHandler = new Facebook;
				$user = $facebookHandler->getUserData($accessToken);
				break;

			case self::GOOGLE:
				$googleHandler = new Google;
				$user = $googleHandler->getUserData($accessToken);
				break;
		}

		return $user;
	}

	public static function registerSocialAccount($socialNetworkIdentifier, User $user, array $data)
	{
		$socialAccount = $user->socialAccounts()->save(new SocialAccount([
            'network' => $socialNetworkIdentifier,
            'account_id' => $data['id'],
            'email' => $data['email'],
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ]));
	}
}