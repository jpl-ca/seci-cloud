<?php

namespace Seci\Libraries;

use GoogleCloudMessaging\GCM;
//use GoogleCloudMessaging\Facades\GCM;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Seci\Models\IncidentAlert;
use Seci\Models\User;
use Seci\Models\UserType;
use Seci\Models\NotificationMessage;
use Seci\Models\NotificationMessageResult;

class Notifier
{
	protected $gcm;

	protected $notificationMessages = [
		'1' => [
			'2' => ['message' => 'Su alerta ha sido notificada a los agentes más cercanos', 'seci_notification_type' => 1],
			'3' => ['message' => 'Uno de los agentes se dirige hacia su ubicación para atender la emergencia', 'seci_notification_type' => 2],
			'4' => ['message' => 'La atención de su alerta ha sido reportada como finalizada', 'seci_notification_type' => 3],
			'5' => ['message' => 'En este momento no hay agentes disponibles para atenderlo', 'seci_notification_type' => 4]
		],
		'2' => [
			'1' => ['message' => 'Una nueva alerta ha sido reportada', 'seci_notification_type' => 5],
			'3' => ['message' => 'La alerta esta siendo atendida por uno de los agentes', 'seci_notification_type' => 6]
		]
	];

	protected $dataMessaged = [
		'citizen' => [
			'1' => [],
			'2' => [],
			'3' => [],
			'4' => [],
			'5' => [],
		],
		'agent' => [
			'1' => [],
			'2' => [],
			'3' => [],
			'4' => [],
			'5' => [],
		]
	];

	public function __construct()
	{
		$serverApiKey = config("gcm.server_api_key");
		$this->gcm = new GCM($serverApiKey);
	}

	public function notify($recivers, $data, $notification)
	{
		return $this->gcm->send([
		//return GCM::send([
        	'to' => $recivers,
        	'data' => $data,
        	'notification' => $notification
        ]);
	}

	public function notifyUsers($reciversData, $userType, $incidentAlertStateId, $incidentAlertId, $data = null)
	{
		if($reciversData instanceof Collection)
		{
			$notifiedUsers = $reciversData->keys()->toArray();
			$recivers = $reciversData->flatten()->toArray();
		}

		if($reciversData instanceof User)
		{
			$notifiedUsers = [$reciversData->id];
			$recivers = [$reciversData->gcm_id];
		}

		$notification = $this->notificationMessages[$userType][$incidentAlertStateId];

		$googleCloudServerResponse = $this->notify($recivers, $data, $notification);

		$notificationMessage = NotificationMessage::create([
			'multicast_id' => $googleCloudServerResponse->multicast_id,
			'success' => $googleCloudServerResponse->success,
			'failure' => $googleCloudServerResponse->failure,
			'canonical_ids' => $googleCloudServerResponse->canonical_ids,
			'notified_user_type_id' => $userType,
			'notified_users' => $notifiedUsers,
			'incident_alert_id' => $incidentAlertId,
			'incident_alert_state_id' => $incidentAlertStateId
		]);

		$results = $googleCloudServerResponse->results;
		$notificationMessagesResultsArray = array();

		foreach ($results as $result) {
			$resultArray = (array)$result;
			foreach ($resultArray as $key => $value) {
				$notificationMessagesResult = new NotificationMessageResult([
					'type' => $key,
					'result' => $value,
					'notification_message_id' => $incidentAlertId
				]);
				array_push($notificationMessagesResultsArray, $notificationMessagesResult);
            }
		}

		if(count($notificationMessagesResultsArray) > 0)
		{
			$notificationMessage->results()->saveMany($notificationMessagesResultsArray);
		}
	}

}