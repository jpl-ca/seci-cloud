<?php

namespace Seci\Libraries;

use Curl\Curl;
use GoogleCloudMessaging\Exception\GoogleCloudMessagingException;
use Seci\Models\User;
use Seci\Libraries\SocialNetworks;

class Facebook
{
	protected $appId;
	protected $appSecret;

	public function __construct()
	{
		$this->appId = config('social.facebook.app_id');
		$this->appSecret = config('social.facebook.app_secret');
		$this->host = "https://graph.facebook.com";
	}

	public function getLongLivedToken($shortLivedToken)
	{
		$url = $this->host . "/oauth/access_token?grant_type=fb_exchange_token&client_id=$this->appId&client_secret=$this->appSecret&fb_exchange_token=$shortLivedToken";
		$curl = new Curl($url);

		$curl->exec();

		if ($curl->error) {
			//throw new GoogleCloudMessagingException($curl->errorMessage, $curl->errorCode);
			//exit();
			return (array)$curl->response->error;
		}
		else {
			$curl->close();
			return $curl->response;
		}
	}

	public function getUserData($accessToken)
	{
		$url = $this->host . "/me?access_token=$accessToken&fields=email,name";
		$curl = new Curl($url);

		$curl->exec();

		if($curl->error){
			return SocialNetworks::INVALID_TOKEN_ERROR;
		}
		else{
			$curl->close();
			return (array)$curl->response;
		}
	}
}