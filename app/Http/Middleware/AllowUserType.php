<?php

namespace Seci\Http\Middleware;

use Closure;
use Seci\Models\User;

class AllowUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $userType)
    {
        if(User::oauth()->user_type_id == $userType)
        {         
            return $next($request);
        }

        return responseJsonForbidden(['message' => 'acceso denegado']);
    }
}
