<?php

namespace Seci\Http\Controllers;

use Illuminate\Http\Request;

use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use Seci\Models\User;
use Seci\Models\UserType;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;

class SiteController extends Controller
{
    public function getIndex()
    {
        return view('site.pages.dashboard.dashboard');
    }
   
    public function getAllData()
    {
        $agents = [
            'free' => User::freeActiveAgentsOnly()->withGcm()->get()->toArray(),
            'unactive' => User::freeUnactiveAgentsOnly()->withGcm()->get()->toArray(),
            'busy' => User::busyAgentsOnly()->withGcm()->get()->toArray()
        ];

        $incidents = IncidentAlert::whereIn('incident_alert_state_id', [
            IncidentAlertState::STATE_NEW,
            IncidentAlertState::STATE_NOTIFIED,
            IncidentAlertState::STATE_BEING_HEEDED
        ])->with(['state', 'type', 'citizen', 'agent'])->get()->toArray();

        $response = [
            'agents'    => $agents,
            'incidents' => $incidents
        ];

        return responseJsonOk($response);
    }
}
