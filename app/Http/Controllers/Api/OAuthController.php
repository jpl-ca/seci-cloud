<?php

namespace Seci\Http\Controllers\Api;

use Illuminate\Http\Request;

use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use \Authorizer;
use Daylight\OAuth\OAuth;
use Seci\OAuth\OAuth as OAuthHandler;
use Daylight\OAuth\Eloquent\OauthAccessToken;
use Daylight\OAuth\Eloquent\OauthClient;
use Seci\Models\User;
use Seci\Models\SocialAccount;
use Seci\Libraries\SocialNetworks;

class OAuthController extends Controller
{
    protected $socialNetworkIdentifiers = ['facebook', 'google'];

    public function __construct()
    {
        $this->middleware('oauth', ['except' => ['postIndex', 'postSocial']]);
    }

    public function postIndex(Request $request)
    {
        $accessData = Authorizer::issueAccessToken();
        $user = User::find(OauthAccessToken::find($accessData['access_token'])->session->owner_id);
        $userData = $user->toCustomArray($user->user_type_id);
        return response()->json(array_merge($accessData, $userData));
    }

    public function deleteIndex(Request $request)
    {
        $user = User::oauth();
        if(!is_null($user))
        {
            $user->gcm_id = null;
            $user->save();
        }
        OAuth::expireRequestSession();
        return responseJsonOk(['message' => 'sesión cerrada']);
    }

    public function postSocial(Request $request, $socialNetworkIdentifier)
    {
        if(! in_array($socialNetworkIdentifier, $this->socialNetworkIdentifiers))
        {
            return responseJsonBadRequest(['message' => 'La red social no existe', 'errors' => ['La red social no existe']]);
        }

        if(! $request->has('social_access_token'))
        {
            return responseJsonBadRequest(['message' => 'No existe un token', 'errors' => ['No existe un token']]);
        }

        if(! $request->has('client_id') && ! $request->has('client_secret') )
        {
            return responseJsonBadRequest(['message' => 'No existe un client_id o client_secret para SeCi', 'errors' => ['No existe un client_id o client_secret para SeCi']]);
        }

        $clientId = $request->get('client_id');
        $clientSecret = $request->get('client_secret');

        $client = OAuthHandler::getClient($clientId, $clientSecret);

        if( $client == null)
        {
            return responseUnauthorized(['message' => 'Credenciales SeCi inválidas', 'errors' => ['Credenciales SeCi inválidas']]);
        }

        $accessToken = $request->get('social_access_token');

        $socialUser = $this->validateSocialAccount($socialNetworkIdentifier, $accessToken);

        if($socialUser == SocialNetworks::INVALID_TOKEN_ERROR)
        {
            return responseJsonBadRequest(['message' => 'Token de red social inválido', 'errors' => ['Token de red social inválido']]);
        }

        if($socialUser instanceof SocialAccount)
        {
            $user = $socialUser->user;
            $userData = $user->toCustomArray($user->user_type_id);

            $accessData = $this->expediteAccessData($user, $client);

            return response()->json(array_merge($accessData, $userData));
        }

        return responseUnauthorized(['message' => 'Usuario no registrado', 'errors' => ['El usuario no está registrado en el sistema']]);
    }

    protected function expediteAccessData(User $user, OauthClient $client)
    {
        $accessToken = OAuthHandler::createSession($client, $user);

        $accessData = [
          "access_token" => $accessToken->id,
            "token_type" => "Bearer",
            "expires_in" => $accessToken->expire_time,
        ];

        return $accessData;
    }

    protected function validateSocialAccount($socialNetworkIdentifier, $accessToken)
    {
        $socialUser = SocialNetworks::getUser($socialNetworkIdentifier, $accessToken);

        if($socialUser != SocialNetworks::INVALID_TOKEN_ERROR)
        {
            $socialAccount = SocialAccount::$socialNetworkIdentifier()
                ->where('email', $socialUser['email'])
                ->where('account_id', $socialUser['id'])
                ->first();

            if(! $socialAccount instanceof SocialAccount)
            {
                $anotherSocialNetworks = SocialNetworks::$REGISTERED_SOCIAL_NETWORKS;

                if(($key = array_search($socialNetworkIdentifier, $anotherSocialNetworks)) !== false) {
                    unset($anotherSocialNetworks[$key]);
                }

                foreach ($anotherSocialNetworks as $socialNetwork) {
                    $socialAccount = SocialAccount::$socialNetwork()
                        ->where('email', $socialUser['email'])
                        ->first();

                    if($socialAccount instanceof SocialAccount)
                    {
                        $user = $socialAccount->user;
                        $socialUser = SocialNetworks::registerSocialAccount($socialNetworkIdentifier, $user, $socialUser);
                        break;
                    }
                }
            }

            return $socialAccount;

        }

        return $socialUser;
    }
}
