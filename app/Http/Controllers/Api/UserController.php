<?php

namespace Seci\Http\Controllers\Api;

use Illuminate\Http\Request;

use Hash;
use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use Daylight\Routing\Api\UserCrudFunctions as ApiCrudFunctions;
use Seci\Models\User;
use Seci\Models\UserType;
use Seci\Libraries\SocialNetworks;

class UserController extends Controller
{
    use ApiCrudFunctions;

    protected $creationRules = [
        'user_type_id' => 'required|integer|exists:user_types,id',
        'first_name' => 'required|min:2|max:255',
        'last_name' => 'required|min:2|max:255',
        'id_number' => 'required|integer|digits:8|unique:users,id_number',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|min:6|max:255',
    ];

    protected $socialCreationRules = [
        'first_name' => 'required|min:2|max:255',
        'last_name' => 'required|min:2|max:255',
        'id_number' => 'required|integer|digits:8|unique:users,id_number',
        'email' => 'required|email|max:255|unique:users,email'
    ];

    protected $updateRules = [
        'lat' => 'numeric',
        'lng' => 'numeric',
        'gcm_id' => 'min:10',
    ];

    public function __construct()
    {
        $citizenType = UserType::TYPE_CITIZEN;
        $agentType = UserType::TYPE_AGENT;
        $this->middleware("oauth", ["except" => ["postIndex"]]);
    }

    public function getIndex()
    {
        $user = User::oauth();
        
        if(in_array($user->user_type_id, [UserType::TYPE_CITIZEN, UserType::TYPE_AGENT]))
        {
            return responseJsonOk(['message' => '', 'data' => $user->toCustomArray($user->user_type_id)]);
        }

        $users = User::all()->toArray();

        return responseJsonOk(['message' => '', 'data' => $users->toArray()]);

    }

    public function postIndex(Request $request)
    {
        $user = User::oauth();

        if($user == null || $user->user_type_id == UserType::TYPE_AGENT)
        {
            $request->offsetSet('user_type_id', UserType::TYPE_CITIZEN);
        }

        if($request->has('social_access_token') && $request->has('social_network') && in_array($request->get('social_network'), [SocialNetworks::FACEBOOK, SocialNetworks::GOOGLE]))
        {
            return $this->socialCreate($request);
        }

        return $this->createOrFail(User::class, $request, true);
    }

    public function putIndex(Request $request)
    {
        //return $this->updateOrFail(User::class. $id, $request, true);
        return $this->update($request);
    }

    public function deleteIndex()
    {

    }

    protected function create(Request $request)
    {        
        $requestData = $request->all();

        $user = new User([
            'user_type_id' => $requestData['user_type_id'],
            'first_name' => $requestData['first_name'],
            'last_name' => $requestData['last_name'],
            'id_number' => $requestData['id_number'],
            'email' => $requestData['email'],
            'password' => Hash::make($requestData['password'])
        ]);

        $user->save();

        return $user;
    }

    protected function update(Request $request)
    {        
        $requestData = $request->all();
        $user = User::oauth();

        foreach ($requestData as $key => $value) {
            if( in_array($key, ['lat','lng','gcm_id']) )
            {
                $user->$key = $value;
            }
        }

        $user->save();

        $response = ['message' => $this->updateSuccessMsg];

        return responseJsonOk($response);
    }

    protected function socialCreate(Request $request)
    {
        $socialNetworkIdentifier = $request->get('social_network');
        $accessToken = $request->get('social_access_token');

        $socialUser = SocialNetworks::getUser($socialNetworkIdentifier, $accessToken);

        if($socialUser === SocialNetworks::INVALID_TOKEN_ERROR)
        {
            return responseJsonBadRequest(['message' => 'Token de red social inválido', 'errors' => ['Token de red social inválido']]);
        }

        $data = $request->only('first_name','last_name','id_number','email');

        $validator = $this->validator($data, $this->socialCreationRules);

        if ($validator->fails()) {
            return responseJsonUnprocessableEntity( ['message' => $this->contentCannotBeParsedMsg, 'errors' => shrinkValidationErrors( $validator->errors()->getMessages() ) ] );
        }

        $data['password'] = str_random(40);

        $user = new User;

        $user->user_type_id = UserType::TYPE_CITIZEN;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->id_number = $data['id_number'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->verified = true;

        $user->save();

        SocialNetworks::registerSocialAccount($socialNetworkIdentifier, $user, $socialUser);

        $response = ['message' => $this->creationSuccessMsg];
        $response['data'] = $user->toArray();
        
        return responseJsonOk( $response );
    }
}
