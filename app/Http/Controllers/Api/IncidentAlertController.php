<?php

namespace Seci\Http\Controllers\Api;

use Illuminate\Http\Request;

use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use Daylight\Routing\Api\CrudFunctions as ApiCrudFunctions;
use Daylight\Routing\ExecutionError;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use Seci\Models\User;
use Seci\Models\UserType;

class IncidentAlertController extends Controller
{
    use ApiCrudFunctions;

    protected $creationRules = [
        'lat' => 'required|numeric',
        'lng' => 'required|numeric',
        'address' => 'required',
        'incident_alert_type_id' => 'required|integer|exists:incident_alert_types,id',
    ];

    protected $updateRules = [
        //'incident_alert_state_id' => 'integer|exists:incident_alert_states,id',
    ];

    public function __construct()
    {
        $citizenType = UserType::TYPE_CITIZEN;
        $agentType = UserType::TYPE_AGENT;
        $this->middleware("oauth");
        $this->middleware("allow.user.type:$citizenType", ["only" => ["postIndex"]]);
    }

    public function getIndex()
    {

    }

    public function postIndex(Request $request)
    {
        return $this->createOrFail(IncidentAlert::class, $request, true);
    }

    public function putIndex(Request $request, $id)
    {
        return $this->updateOrFail(IncidentAlert::class, $id, $request, true);
    }

    public function deleteIndex()
    {

    }

    protected function create(Request $request)
    {
        $requestData = $request->all();
        $user = User::oauth();

        $incidentAlert = new IncidentAlert([
            'user_id' => $user->id,
            'id_number' => $user->id_number,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'lat' => $requestData['lat'],
            'lng' => $requestData['lng'],
            'address' => $requestData['address'],
            'incident_alert_type_id' => $requestData['incident_alert_type_id']
        ]);

        $incidentAlert->save();

        return $incidentAlert;
    }

    protected function update(Request $request, $id)
    {
        $requestData = $request->all();
        $agent = User::oauth();

        $incidentAlert = IncidentAlert::find($id);

        if( $incidentAlert->agent_id == null && $incidentAlert->incident_alert_state_id == IncidentAlertState::STATE_NOTIFIED )
        {
            \Log::info('entro al primer if');
            $incidentAlert->agent_id = $agent->id;
            $incidentAlert->incident_alert_state_id = IncidentAlertState::STATE_BEING_HEEDED;
            $incidentAlert->save();
        }
        else
        {
            \Log::info('entro al else');
            if( $incidentAlert->agent_id == $agent->id && $incidentAlert->incident_alert_state_id == IncidentAlertState::STATE_BEING_HEEDED )
            {
                \Log::info('entro al segundo if');
                $incidentAlert->incident_alert_state_id = IncidentAlertState::STATE_FINISHED;
                $incidentAlert->agent_review = $request->get('agent_review');
                $incidentAlert->save();
            }
            else
            {
                return new ExecutionError('Este incidente ya esta siendo atendido por otro agente', $incidentAlert);
            }
        }

        return $incidentAlert;

    }
}
