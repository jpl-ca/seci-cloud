<?php

namespace Seci\Http\Controllers\Api;

use Illuminate\Http\Request;

use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use Daylight\Routing\Api\UserCrudFunctions;
use Seci\Models\User;
use Hash;

class AuthController extends Controller
{
    use UserCrudFunctions;

    public $rules = [
        'first_name'    => 'required|max:255',
        'last_name'     => 'required|max:255',
        'id_number'     => 'required|integer|unique:users,id_number',
        'email'         => 'required|email|unique:users,email',
        'password'      => 'required|min:6',
    ];

    public function getIndex()
    {
        return 1;
    }

    public function postIndex(Request $request)
    {
        $requestData = $request->all();
        return $this->createOrFail(User::class, $request);
    }

    protected function create(Request $request)
    {
        $data = $request->all();

        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'id_number' => $data['id_number'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        return $user;
    }

}
