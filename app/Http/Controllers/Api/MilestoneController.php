<?php

namespace Seci\Http\Controllers\Api;

use Illuminate\Http\Request;

use Hash;
use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;
use Daylight\Routing\Api\UserCrudFunctions as ApiCrudFunctions;
use Seci\Models\Milestone;
use Seci\Models\MilestoneType;

class MilestoneController extends Controller
{
    use ApiCrudFunctions;

    protected $creationRules = [
        'name' => 'required|min:2|max:255',
        'lat' => 'required|numeric',
        'lng' => 'required|numeric',
        'milestone_type_id' => 'required|integer|exists:milestone_types,id',
    ];

    protected $updateRules = [
        'name' => 'required|min:2|max:255',
        'lat' => 'required|numeric',
        'lng' => 'required|numeric',
        'milestone_type_id' => 'required|integer|exists:milestone_types,id',
    ];

    public function __construct()
    {
        $this->middleware("oauth", ["except" => []]);
    }

    public function getIndex(Request $request)
    {
        $milestones = $this->handleQueries($request, Milestone::class);
        
        if($milestones == null)
        {
            $milestones = Milestone::with(['type'])->get();
        }

        return responseJsonOk(['message' => '', 'data' => $milestones->toArray()]);

    }

    function handleQueries(Request $request, $modelClass)
    {
        if(!$request->has('q'))
        {
            return null;
        }
        $query = $request->get('q');
        $queryParameters = explode(",", $query);
        if(count($queryParameters) == 3)
        {
            return $modelClass::where($queryParameters[0], $queryParameters[1], $queryParameters[2])->get();
        }
        return null;
    }
}
