<?php

namespace Seci\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Seci\Models\User;
use Validator;
use Seci\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Daylight\Support\Facades\Confirmation;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/';
    protected $redirectAfterLogout = 'auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getConfirm($token = null)
    {
        if (!$token) {
            return view('auth.confirm', ['error' => trans(Confirmation::NULL_TOKEN)]);
        }

        $credentials = ['token' => $token];

        $response = Confirmation::confirm($credentials, function ($user) {
            $this->confirmAccount($user);
        });

        switch ($response) {
            case Confirmation::ACCOUNT_CONFIRMATION:
                return view('auth.confirm', ['success' => trans($response)]);

            default:
                return view('auth.confirm', ['warning' => trans($response)]);

        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Daylight\Contracts\Auth\CanConfirmAccunt  $user
     * @return void
     */
    protected function confirmAccount($user)
    {
        $user->verified = true;
        $user->save();
    }
}
