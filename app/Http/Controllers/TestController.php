<?php

namespace Seci\Http\Controllers;

use Illuminate\Http\Request;

use Seci\Http\Requests;
use Seci\Http\Controllers\Controller;

class TestController extends Controller
{
    /**
     * Case 1:
     * Testing the correct function of \Sesi\Events\NotifiNearlyAgents
     * to prove that the Push notifications are delivered successfully.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCase1()
    {
        $incidentAlert = new \Seci\Models\IncidentAlert;
        $user = \Seci\Models\User::find(1);

        $incidentAlert->user_id = $user->id;
        $incidentAlert->id_number = $user->id_number;
        $incidentAlert->first_name = $user->first_name;
        $incidentAlert->last_name = $user->last_name;
        $incidentAlert->lat = -12.088545;
        $incidentAlert->lng = -77.016186;
        $incidentAlert->incident_alert_type_id = 1;

        $incidentAlert->save();

        return $incidentAlert->toArray();
    }
    /**
     * Case 2:
     * Testing the correct function of \Sesi\Models\Users
     * to prove that the function can find nearby agents
     * to a given coordinate.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCase2()
    {
        $radiusCenter = new \Location\Coordinate(-12.088545, -77.016186);
        $radius = 10000; //radius in meters

        $nearbyAgents = \Seci\Models\User::getFreeAgentsInRadius($radiusCenter, $radius);

        dd(collect($nearbyAgents->toArray())->keyBy('id')->pluck('lat')->toArray());
    }

    public function getCase0()
    {
        //return dd(\League\OAuth2\Server\Util\SecureKey::generate());
        //exit();
        //$llt = "CAAX8IdZAtIfMBADBioxPlvxZCNGdHwqVB8HEfsJj98vGibSZAd4K4Eq448PXrVGr8gGZAR9XPYm8cdZCw8eiV2dN0JXi7m5jmEmeZB0vXEFezFqZASXSZCpBMvTGpYZAAJZC2krn6CILSIkpyJkxYEIeEaJb82OdxTrT7eZAbZCu8jc8n1kmFgCEPSJZC";
        $slt = "ya29.PwLBg7M5h4YJd5WPE2LTs3HPdftMnBGrhXGF29M_4UJes6r-G8dxuvTTzIsb_8o5KvTX";
        $gp = new \Seci\Libraries\Google;
        return dd($gp->getUserData($slt));
        //return $fb->getLongLivedToken('CAAX8IdZAtIfMBAPmCEl9pYPGEirvKizpvck6jYYZC1HM4WMpOlho2txaxA1ZAKPhBoaoU42F3wf1XNZAgHshbzxN65AtZCQwf9MlrgtcB0IfTPZBBgRG42R220A9WbM4bFFc8HtpdyDjJvNNqkEA15Sq7l7eNEjTArwIEQAk3jrXG9bwXucG4F0TBOVUHaPvypQV9GzZAcivPTxyfNUqEAmZCBWt1jfStsc5mJwvKBIHnAZDZD');

    }
}
