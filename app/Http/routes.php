<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return base_path();
});*/

//Route::controller('test', 'Api\AuthController');

Route::controller('test', 'TestController');

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function(){
	Route::controller('oauth', 'OAuthController');
	Route::controller('user', 'UserController');
	Route::put('incident-alert/{id}', 'IncidentAlertController@putIndex');
	Route::controller('incident-alert', 'IncidentAlertController');
	Route::controller('milestone', 'MilestoneController');
});

Route::controller('auth', 'Auth\AuthController');

Route::group(['middleware' => 'auth'], function(){
	Route::controller('/', 'SiteController');
});
