<?php

namespace Seci\Listeners;

use Seci\Events\NotificationMessageWereSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Seci\Models\NotificationMessage;
use Seci\Models\NotificationMessageResult;

class NotificationMessageWereSend
{
    public $googleCloudServerResponse;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationMessageWereSend  $event
     * @return void
     */
    public function handle(NotificationMessageWereSend $event)
    {
        $this->googleCloudServerResponse = $event->googleCloudServerResponse;
    }
}
