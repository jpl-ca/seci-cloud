<?php

namespace Seci\Listeners;

use Seci\Events\IncidentAlertWasCreatedOrUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Seci\Models\UserType;
use Seci\Models\User;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use Seci\Models\IncidentAlertHistory;
use Seci\Libraries\Notifier;
use Location\Coordinate;

class IncidentAlertWasCreatedOrUpdatedListener implements ShouldQueue
{
    use InteractsWithQueue;

    public $incidentAlert;
    public $reporterCitizen;

    protected $notifier;

    private $searchRadius;
    private $searchRetries;
    private $expansionRate;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->searchRadius = config('seci.search_radius');
        $this->searchRetries = config('seci.search_retries');
        $this->expansionRate = config('seci.expansion_rate');
        $this->notifier = new \Seci\Libraries\Notifier;
    }

    /**
     * Handle the event.
     *
     * @param  IncidentAlertWasCreatedOrUpdated  $event
     * @return void
     */
    public function handle(IncidentAlertWasCreatedOrUpdated $event)
    {
        $this->incidentAlert = $event->incidentAlert;
        $this->reporterCitizen = $this->incidentAlert->citizen;

        $this->createIncidentAlertHistoryRecord();

        switch ($this->incidentAlert->incident_alert_state_id) {
            case IncidentAlertState::STATE_NEW:
                # notificar a todos los agentes disponibles cercanos al punto de la alerta
                $this->handleNewIncidentAlert();
                break;
            case IncidentAlertState::STATE_NOTIFIED:
                # notificar al ciudadano que su alerta esta ha sido notificada
                $this->handleNotifiedIncidentAlert();
                break;
            case IncidentAlertState::STATE_BEING_HEEDED:
                # notificar a todos los agentes que fueron notificados con anterioridad que ya la alerta esta siendo atendida por uno de los agentes
                # notificar al ciudadano que un agente esta en camino para atender su alerta
                $this->handleBeingHeededIncidentAlert();
                break;
            case IncidentAlertState::STATE_FINISHED:
                # notificar al ciudadano que la atencion de su alerta ha terminado
                $this->handleFinishedIncidentAlert();
                break;
            case IncidentAlertState::STATE_CANCELLED:
                # notificar al ciudadano que la atención de su alerta ha sido cancelada
                $this->handleCancelledIncidentAlert();
                break;
        }

        return true;
        
    }

    private function createIncidentAlertHistoryRecord()
    {
        $incidentAlertHistory = $this->incidentAlert->history()->save(new IncidentAlertHistory([
            'incident_alert_state_id' => $this->incidentAlert->incident_alert_state_id,
            'created_at' => $this->incidentAlert->created_at,
            'updated_at' => $this->incidentAlert->updated_at,
        ]));
    }

    private function findNearbyAgentsToIncident(Coordinate $radiusCenter)
    {
        $searchAtempt = 0;
        $searchRadius = $this->searchRadius;

        while($searchAtempt <= $this->searchRetries)
        {
            $nearbyAgents = \Seci\Models\User::getFreeAgentsInRadius($radiusCenter, $searchRadius);

            if(count($nearbyAgents) > 0)
            {
                break;
            }
            else{
                $searchAtempt++;
                $searchRadius += $this->expansionRate;
            }
        }
        
        return $nearbyAgents;
    }

    private function handleNewIncidentAlert()
    {
        $radiusCenter = new Coordinate($this->incidentAlert->lat, $this->incidentAlert->lng);

        $nearbyAgents = $this->findNearbyAgentsToIncident($radiusCenter);

        if($nearbyAgents->isEmpty())
        {
            $this->incidentAlert->incident_alert_state_id = IncidentAlertState::STATE_CANCELLED;
            $this->incidentAlert->save();
        }
        else
        {
            $reciversData = collect($nearbyAgents->toArray())->pluck('gcm_id','id');
            
            $this->notifier->notifyUsers($reciversData, UserType::TYPE_AGENT, IncidentAlertState::STATE_NEW, $this->incidentAlert->id, $this->incidentAlert->toArray());

            $this->incidentAlert->incident_alert_state_id = IncidentAlertState::STATE_NOTIFIED;
            $this->incidentAlert->save();
        }
    }

    private function handleNotifiedIncidentAlert()
    {
        $this->notifier->notifyUsers($this->reporterCitizen, UserType::TYPE_CITIZEN, IncidentAlertState::STATE_NOTIFIED, $this->incidentAlert->id);
    }

    private function handleBeingHeededIncidentAlert()
    {
        $previousNotifiedAgentsIds = $this->incidentAlert->notificationMessages()->where('notified_user_type_id', \Seci\Models\UserType::TYPE_AGENT)->where('incident_alert_state_id', \Seci\Models\IncidentAlertState::STATE_NEW)->first()->notified_users;
        
        if(($key = array_search($this->incidentAlert->agent_id, $previousNotifiedAgentsIds)) !== false) {
            unset($previousNotifiedAgentsIds[$key]);
        }

        $previousNotifiedAgents = User::whereIn('id', $previousNotifiedAgentsIds)->get();

        if($previousNotifiedAgents->count() > 0)
        {
            $reciversData = collect($previousNotifiedAgents->toArray())->pluck('gcm_id','id');

            $this->notifier->notifyUsers($reciversData, UserType::TYPE_AGENT, IncidentAlertState::STATE_BEING_HEEDED, $this->incidentAlert->id, $this->incidentAlert->toArray());
        }

        $this->notifier->notifyUsers($this->reporterCitizen, UserType::TYPE_CITIZEN, IncidentAlertState::STATE_BEING_HEEDED, $this->incidentAlert->id);
    }

    private function handleFinishedIncidentAlert()
    {
        $this->notifier->notifyUsers($this->reporterCitizen, UserType::TYPE_CITIZEN, IncidentAlertState::STATE_FINISHED, $this->incidentAlert->id);
    }

    private function handleCancelledIncidentAlert()
    {
        $this->notifier->notifyUsers($this->reporterCitizen, UserType::TYPE_CITIZEN, IncidentAlertState::STATE_CANCELLED, $this->incidentAlert->id);
    }
}
