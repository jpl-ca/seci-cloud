<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\IncidentAlert;

class IncidentAlertType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    
    public function incidentAlerts()
    {
    	return $this->hasMany(IncidentAlert::class, 'incident_alert_type_id', 'id');
    }
}
