<?php

namespace Seci\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Daylight\Auth\Accounts\CanConfirmAccount;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Daylight\Contracts\Auth\CanConfirmAccount as CanConfirmAccountContract;
use Seci\Models\UserType;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use Seci\Models\SocialAccount;
use Location\Coordinate;
use Daylight\OAuth\OAuth;
use Seci\Libraries\Geo;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    CanConfirmAccountContract
{
    use Authenticatable, Authorizable, CanResetPassword, CanConfirmAccount;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_type_id', 'first_name', 'last_name', 'id_number', 'email', 'password', 'lat', 'lng', 'verified'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified' => 'boolean',
    ];

    /**
     * Scope a query to only include user with gcm.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithGcm($query)
    {
        return $query->whereNotNull('gcm_id');
    }

    /**
     * Scope a query to only include user with position.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithPosition($query)
    {
        return $query->whereNotNull('lat')->whereNotNull('lng');
    }

    /**
     * Scope a query to only include citizens.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCitizensOnly($query)
    {
        return $query->where('user_type_id', UserType::TYPE_CITIZEN);
    }

    /**
     * Scope a query to only include agents.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAgentsOnly($query)
    {
        return $query->where('user_type_id', UserType::TYPE_AGENT);
    }

    /**
     * Scope a query to only include busy agents.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBusyAgentsOnly($query)
    {
        $busyAgents = IncidentAlert::whereNotNull('agent_id')
            ->where('incident_alert_state_id', IncidentAlertState::STATE_BEING_HEEDED)
            ->get(['agent_id']);
        $busyAgents = $busyAgents->count() > 0 ? $busyAgents->toArray() : array();
        return $query->agentsOnly()->whereIn('id', $busyAgents);
    }

    /**
     * Scope a query to only include free agents.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFreeAgentsOnly($query)
    {
        $busyAgents = self::busyAgentsOnly()->get(['id']);
        $busyAgents = $busyAgents->count() > 0 ? $busyAgents->toArray() : array();

        return $query->agentsOnly()->whereNotIn('id', $busyAgents);
    }

    /**
     * Scope a query to only include free agents.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFreeActiveAgentsOnly($query)
    {
        $lastMinutes = Carbon::now()->subMinutes(30)->toDateTimeString();

        return $query->freeAgentsOnly()->where('updated_at', '>=', $lastMinutes);
    }

    /**
     * Scope a query to only include free agents.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFreeUnactiveAgentsOnly($query)
    {
        $lastMinutes = Carbon::now()->subMinutes(30)->toDateTimeString();

        return $query->freeAgentsOnly()->where('updated_at', '<=', $lastMinutes);
    }

    /**
     * Scope a query to only include operators.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOperatorsOnly($query)
    {
        return $query->where('user_type_id', UserType::TYPE_WEB_OPERATOR);
    }

    /**
     * Scope a query to only include administrators.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdministratorsOnly($query)
    {
        return $query->where('user_type_id', UserType::TYPE_WEB_ADMINISTRATOR);
    }

    public function type()
    {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }    

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class, 'user_id', 'id');
    }

    public function getCoordinate()
    {
        return (is_null($this->lat) || is_null($this->lng)) ? null : new Coordinate($this->lat, $this->lng);
    }

    public static function oauth()
    {
        $requestSession = OAuth::getRequestSession();
        $oAuthUserId = is_null($requestSession) ? $requestSession : $requestSession->getOwnerId();
        return self::find($oAuthUserId);
    }

    public function toCustomArray($user_type)
    {
        $array = parent::toArray();

        switch ($user_type) {
            case UserType::TYPE_CITIZEN:
                unset($array['id']);
                unset($array['verified']);
                unset($array['lat']);
                unset($array['lng']);
                unset($array['gcm_id']);
                unset($array['created_at']);
                unset($array['updated_at']);
                unset($array['user_type_id']);
                break;
            
            case UserType::TYPE_AGENT:
                unset($array['id']);
                unset($array['verified']);
                unset($array['lat']);
                unset($array['lng']);
                unset($array['created_at']);
                unset($array['updated_at']);
                unset($array['user_type_id']);
                break;
        }

        return $array;
    }

    public static function getFreeAgentsInRadius(Coordinate $radiusCenter, $radius)
    {
        $agents = self::freeAgentsOnly()->withGcm()->get();

        foreach ($agents as $key => $agent) {
            $inRadius = ( $agent->lat == null ) ? false : Geo::coordinateInRadius($radiusCenter, $radius, $agent->getCoordinate());
            if(!$inRadius)
            {
                $agents->forget($key);
            }
        }

        return $agents;
    }
}