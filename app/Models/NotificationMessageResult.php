<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\NotificationMessage;

class NotificationMessageResult extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['type','result','notification_message_id'];


    public function incidentAlert()
    {
        return $this->belongsTo(NotificationMessage::class, 'notification_message_id', 'id');
    }
}
