<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\User;
use Seci\Libraries\SocialNetworks;

class SocialAccount extends Model
{
    protected $fillable = ['network', 'account_id', 'email', 'created_at', 'updated_at'];
	
    /**
     * Scope a query to only include facebook accounts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFacebook($query)
    {
        return $query->where('network', SocialNetworks::FACEBOOK);
    }

    /**
     * Scope a query to only include google accounts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGoogle($query)
    {
        return $query->where('network', SocialNetworks::GOOGLE);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
