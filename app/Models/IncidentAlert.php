<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\IncidentAlertType;
use Seci\Models\IncidentAlertState;
use Seci\Models\IncidentAlertHistory;
use Seci\Models\User;
use Seci\Models\NotificationMessage;
use Location\Coordinate;

class IncidentAlert extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','id_number','first_name','last_name','lat','lng','address','incident_alert_type_id'];

    public function type()
    {
        return $this->belongsTo(IncidentAlertType::class, 'incident_alert_type_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(IncidentAlertState::class, 'incident_alert_state_id', 'id');
    }

    public function history()
    {
        return $this->hasMany(IncidentAlertHistory::class, 'incident_alert_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id', 'id');
    }

    public function citizen()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function notificationMessages()
    {
    	return $this->hasMany(NotificationMessage::class, 'incident_alert_id', 'id');
    }

    public function getCoordinate()
    {
        return new Coordinate($this->lat, $this->lng);
    }
}
