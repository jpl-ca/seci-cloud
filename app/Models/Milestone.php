<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\MilestoneType;

class Milestone extends Model
{
    protected $fillable = ['name','lat','lng','milestone_type_id','address'];

    public function type()
    {
        return $this->belongsTo(MilestoneType::class, 'milestone_type_id', 'id');
    }
}
