<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\IncidentAlert;

class IncidentAlertHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['incident_alert_id', 'incident_alert_state_id'];
    
    public function incidentAlert()
    {
        return $this->belongsTo(IncidentAlert::class, 'incident_alert_id', 'id');
    }
}
