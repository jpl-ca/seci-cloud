<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\IncidentAlert;

class IncidentAlertState extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Incident alert state constants
     */
    const STATE_NEW = 1;
    const STATE_NOTIFIED = 2;
    const STATE_BEING_HEEDED = 3;
    const STATE_FINISHED = 4;
    const STATE_CANCELLED = 5;
    
    public function incidentAlerts()
    {
    	return $this->hasMany(IncidentAlert::class, 'incident_alert_state_id', 'id');
    }
}
