<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\UserType;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use Seci\Models\NotificationMessageResult;

class NotificationMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['multicast_id','success','failure','canonical_ids','notified_user_type_id','notified_users','incident_alert_id','incident_alert_state_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'notified_users' => 'array',
    ];

    public function notifiedUsertype()
    {
        return $this->belongsTo(UserType::class, 'notified_user_type_id', 'id');
    }

    public function incidentAlert()
    {
        return $this->belongsTo(IncidentAlert::class, 'incident_alert_id', 'id');
    }

    public function incidentAlertState()
    {
        return $this->belongsTo(IncidentAlertState::class, 'incident_alert_state_id', 'id');
    }

    public function results()
    {
        return $this->hasMany(NotificationMessageResult::class, 'notification_message_id', 'id');
    }
}
