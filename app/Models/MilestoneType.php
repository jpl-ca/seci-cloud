<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;

class MilestoneType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Incident alert state constants
     */
    const TYPE_POLICE_AND_SERENAZGO = 1;
    const TYPE_HOSPITALS_AND_CLINICS = 2;
}
