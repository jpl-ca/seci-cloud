<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentAlertNotification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    
}
