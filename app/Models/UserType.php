<?php

namespace Seci\Models;

use Illuminate\Database\Eloquent\Model;
use Seci\Models\User;

class UserType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * User type contants
     */
    const TYPE_CITIZEN = 1;
    const TYPE_AGENT = 2;
    const TYPE_WEB_OPERATOR = 3;
    const TYPE_WEB_ADMINISTRATOR = 4;

    public function users()
    {
    	return $this->hasMany(User::class, 'user_type_id', 'id');
    }
}
