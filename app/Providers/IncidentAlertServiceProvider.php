<?php

namespace Seci\Providers;

use Event;
use Illuminate\Support\ServiceProvider;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use Seci\Events\IncidentAlertWasCreatedOrUpdated;

class IncidentAlertServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        IncidentAlert::creating(function ($incidentAlert) {
            $incidentAlert->incident_alert_state_id = IncidentAlertState::STATE_NEW;
        });

        IncidentAlert::created(function ($incidentAlert) {
            Event::fire(new IncidentAlertWasCreatedOrUpdated($incidentAlert));
        });

        IncidentAlert::updated(function ($incidentAlert) {
            Event::fire(new IncidentAlertWasCreatedOrUpdated($incidentAlert));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
