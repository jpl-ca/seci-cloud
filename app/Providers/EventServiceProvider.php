<?php

namespace Seci\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Seci\Events\IncidentAlertWasCreatedOrUpdated' => [
            'Seci\Listeners\IncidentAlertWasCreatedOrUpdatedListener',
        ],
        'Seci\Events\NotificationMessageWereSend' => [
            'Seci\Listeners\NotificationMessageWereSend',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
