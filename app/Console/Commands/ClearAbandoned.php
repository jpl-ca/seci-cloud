<?php

namespace Seci\Console\Commands;

use Illuminate\Console\Command;
use Seci\Models\IncidentAlert;
use Seci\Models\IncidentAlertState;
use DB;
use Carbon\Carbon;

class ClearAbandoned extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seci:clear-abandoned';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel all abandoned incident alerts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

        $abandonedIncidentsIds = IncidentAlert::where('incident_alert_state_id', IncidentAlertState::STATE_NOTIFIED)
            ->where('updated_at', '<=', $date)
            ->get(['id']);

        $abandonedIncidents = IncidentAlert::where('incident_alert_state_id', IncidentAlertState::STATE_NOTIFIED)
            ->where('updated_at', '<=', $date)
            ->get();

        DB::table('incident_alerts')
            ->whereIn('id', $abandonedIncidentsIds)
            ->update(['incident_alert_state_id' => IncidentAlertState::STATE_CANCELLED]);

        $notifier = new \Seci\Libraries\Notifier;

        foreach ($abandonedIncidents as $incident) {
            $notifier->notifyUsers($incident->citizen, UserType::TYPE_CITIZEN, IncidentAlertState::STATE_CANCELLED, $incident->id);
        }

    }
}
