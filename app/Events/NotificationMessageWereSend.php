<?php

namespace Seci\Events;

use Seci\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationMessageWereSend extends Event
{
    use SerializesModels;

    public $googleCloudServerResponse;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(stdClass $googleCloudServerResponse)
    {
        $this->googleCloudServerResponse = $googleCloudServerResponse;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
