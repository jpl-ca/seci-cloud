<?php

namespace Seci\Events;

use Seci\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IncidentAlertWasCreatedOrUpdated extends Event
{
    use SerializesModels;

    public $incidentAlert;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($incidentAlert)
    {
        $this->incidentAlert = $incidentAlert;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
