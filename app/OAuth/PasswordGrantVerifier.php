<?php 

namespace Seci\OAuth;

use Illuminate\Support\Facades\Auth;

class PasswordGrantVerifier
{
  public function verify($idNumber, $password)
  {
      $credentials = [
        'id_number'=> $idNumber,
        'password' => $password
      ];

      if (Auth::once($credentials)) {
          return Auth::user()->id;
      }

      return false;
  }
}