<?php 

namespace Seci\OAuth;

use Seci\Models\User;
use Daylight\OAuth\Eloquent\OauthClient;
use Daylight\OAuth\Eloquent\OauthSession;
use Daylight\OAuth\Eloquent\OauthAccessToken;
use League\OAuth2\Server\Util\SecureKey;

class OAuth
{
  public static function getClient($clientId, $clientSecret)
  {
  	return OauthClient::where('id', $clientId)->where('secret', $clientSecret)->first();
  }

  public static function createSession(OauthClient $client, User $user)
  {

  	$session = new OauthSession;
  	$session->client_id = $client->id;
  	$session->owner_type = 'user';
  	$session->owner_id = $user->id;
  	$session->save();

  	$tokenId = SecureKey::generate();

  	$accessToken = $session->accessToken()->save(new OauthAccessToken([
  		'id' => $tokenId,
  		'session_id' => $session->id,
  		'expire_time' => config('oauth2.grant_types.password.access_token_ttl') + time()
  	]));

  	$accessToken = OauthAccessToken::where('id', $tokenId)->first();

  	return $accessToken;

  }
}