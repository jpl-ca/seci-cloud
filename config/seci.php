<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Search Radius
    |--------------------------------------------------------------------------
    |
    | Here you have to set search radius in meters used to find nearby agents
    | to an incident alert origin point.
    |
    */

    'search_radius'    => 1000,

    /*
    |--------------------------------------------------------------------------
    | Search Retries
    |--------------------------------------------------------------------------
    |
    | Here you have to set the number of times that the search will retry
    | to find agents expanding the search radius.
    |
    */

    'search_retries'	=> 2,

    /*
    |--------------------------------------------------------------------------
    | Search Radius Expansion Rate
    |--------------------------------------------------------------------------
    |
    | Here you have to set the search radius expansion rate in meters.
    |
    */

    'expansion_rate'    => 500,
];