@extends('site.layouts.master')

@section('sidebar')
	@include('site.partials.sidebar')
@stop

@section('page-content')

    <div id="page-wrapper">
        <div class="container-fluid">
    		<div class="row">
		        <div class="col-lg-12">
		            <h1 class="page-header">{{ isset($title) ? $title : 'Título' }}</h1>
		            @yield('content')
		        </div>
		    </div>
        </div>
    </div>

@stop