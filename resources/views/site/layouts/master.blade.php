@extends('site.layouts.essential')

@section('html')
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('images/seci-banner--horizontal.png') }}" alt="SECI" height=25>
                </a>
            </div>
            <!-- /.navbar-header -->

            @include('site.partials.navbar')

            <!-- /.navbar-top-links -->

            @yield('sidebar')

            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        @yield('page-content')
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop