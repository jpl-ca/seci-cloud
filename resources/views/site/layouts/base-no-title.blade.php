@extends('site.layouts.master')

@section('sidebar')
	@include('site.partials.sidebar')
@stop

@section('page-content')

    <div id="page-wrapper">
        <div class="container-fluid">
    		@yield('content')
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@stop