@extends('site.layouts.base-fullwidth')

@section('main-container')
	<div class="row">
	    <div id="map-canvas" class="full-width-div"></div>
	</div>
@stop

@section('js')

	<script type="text/javascript"
    	src="https://maps.googleapis.com/maps/api/js?v=3.20&key=AIzaSyDCkqbJEOPh-HTDmh1QK2O_xgatY-R0Iu4">
    </script>

	<script type="text/javascript">

		var map;
		var agents = [];
		var incidents = [];

		var options = {
			center: {lat: -12.050725, lng: -77.036260},
			zoom: 13,
			styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}]
		};

		map = new google.maps.Map(document.getElementById('map-canvas'), options);

		function createMarker(lat,lng, imageUrl, infoWindowContent)
		{
			var infowindow = new google.maps.InfoWindow({
				content: infoWindowContent
			});

			var marker = new google.maps.Marker({
			    position: new google.maps.LatLng(lat, lng),
			    icon: imageUrl
			});

			marker.addListener('click', function() {
			    infowindow.open(map, marker);
			});

			return marker;
		}


		function addAgentMarker(lat, lng, imageUrl, infoWindowContent)
		{
			var marker = createMarker(lat,lng, imageUrl, infoWindowContent);

			agents.push(marker);
		}

		function setAgentsOnMap(map)
		{
			for (var i = 0; i < agents.length; i++) {
				agents[i].setMap(map);
			}
		}

		function clearAgents()
		{
			setAgentsOnMap(null);
		}

		function showAgents()
		{
			setAgentsOnMap(map);
		}

		function deleteAgents()
		{
			clearAgents();
			agents = [];
		}

		function addIncidentMarker(lat, lng, imageUrl, infoWindowContent)
		{
			var marker = createMarker(lat,lng, imageUrl, infoWindowContent);

			incidents.push(marker);
		}

		function setIncidentsOnMap(map)
		{
			for (var i = 0; i < incidents.length; i++) {
				incidents[i].setMap(map);
			}
		}

		function clearIncidents()
		{
			setIncidentsOnMap(null);
		}

		function showIncidents()
		{
			setIncidentsOnMap(map);
		}

		function deleteIncidents()
		{
			clearIncidents();
			incidents = [];
		}

		function handleAgents(agents)
		{
			freeAgents = agents.free;
			busyAgents = agents.busy;
			unactiveAgents = agents.unactive;

			$.each( freeAgents, function( key, agent ) {
				addAgentMarker(agent.lat, agent.lng, "{{ asset('images/markers/agente_libre.png') }}", getAgentMarkerDescription(agent, 'Libre'));
			});

			$.each( busyAgents, function( key, agent ) {
				addAgentMarker(agent.lat, agent.lng, "{{ asset('images/markers/agente_ocupado.png') }}", getAgentMarkerDescription(agent, 'Ocupado'));
			});

			$.each( unactiveAgents, function( key, agent ) {
				addAgentMarker(agent.lat, agent.lng, "{{ asset('images/markers/agente_no_actualiza.png') }}", getAgentMarkerDescription(agent, 'Inactivo'));
			});

			showAgents();
		}

		function handleIncidents(incidents)
		{
			$.each( incidents, function( key, incident ) {
				addIncidentMarker(incident.lat, incident.lng, getIncidentImageUrl(incident.incident_alert_type_id, incident.incident_alert_state_id), getIncidentMarkerDescription(incident));
			});

			showIncidents();
		}

		function getIncidentImageUrl(incidenteTypeId, incidentStateId)
		{
			var imageUrl = "{{ asset('images/markers') }}";
			var image = '/' + incidenteTypeId + '_' + incidentStateId + '.png';
			imageUrl += image;
			return imageUrl;
		}

		function getIncidentMarkerDescription(incident)
		{
			var description = '';

			description += "<h4 style='text-decoration:underline;'>Alerta #"+incident.id+"</h4>";
			description += "<p>";
			description += "<strong>Tipo: </strong> "+incident.type.name+"<br>";
			description += "<strong>Estado: </strong> "+incident.state.name+"<br>";
			description += "<strong>Dirección: </strong> "+incident.address+"<br>";
			description += "<strong>Fecha: </strong> "+incident.created_at+"<br>";
			description += "</p>";

			return description;
		}

		function getAgentMarkerDescription(agent, status)
		{
			var description = '';

			description += "<h4 style='text-decoration:underline;'>Agente #"+agent.id+"</h4>";
			description += "<p>";
			description += "<strong>Apellidos: </strong> "+agent.last_name+"<br>";
			description += "<strong>Nombres: </strong> "+agent.first_name+"<br>";
			var estado = status;
			description += "<strong>Estado: </strong> "+ estado +"<br>";
			description += "</p>";

			return description;
		}

		function loadData()
		{
			clearIncidents();
			deleteIncidents();
			clearAgents();
			deleteAgents();

			$.ajax({
				url: "{{ url('all-data') }}"
			}).done(function(response) {
			 	handleAgents(response.agents);
			 	handleIncidents(response.incidents);
			});
		}

		loadData();

		setInterval(function(){ 
			console.log('data has been reloaded: ' + getDateTime());
			loadData();
		}, 20000);

		function getDateTime() {
		    var now     = new Date(); 
		    var year    = now.getFullYear();
		    var month   = now.getMonth()+1; 
		    var day     = now.getDate();
		    var hour    = now.getHours();
		    var minute  = now.getMinutes();
		    var second  = now.getSeconds(); 
		    if(month.toString().length == 1) {
		        var month = '0'+month;
		    }
		    if(day.toString().length == 1) {
		        var day = '0'+day;
		    }   
		    if(hour.toString().length == 1) {
		        var hour = '0'+hour;
		    }
		    if(minute.toString().length == 1) {
		        var minute = '0'+minute;
		    }
		    if(second.toString().length == 1) {
		        var second = '0'+second;
		    }   
		    var dateTime = year+'/'+month+'/'+day+' '+hour+':'+minute+':'+second;   
		     return dateTime;
		}

    </script>
@stop