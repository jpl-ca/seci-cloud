@extends('site.layouts.essential')

@section('html')
    <div class="container">
        <div class="row">
            @if(isset($error))
            <div class="alert alert-danger text-center">
                {{ $error }}
            </div>
            @endif

            @if(isset($success))
            <div class="alert alert-success text-center">
                {{ $success }}
            </div>
            @endif

            @if(isset($warning))
            <div class="alert alert-warning text-center">
                {{ $warning }}
            </div>
            @endif

            <div class="col-md-4 col-md-offset-4">            
                <p class="text-center" id="logo-p-wrapper">
                    <img class="img" src="{{ asset('images/seci-banner--horizontal.png') }}" alt="" style="width:250px">
                </p>
            </div>
        </div>
    </div>
@stop