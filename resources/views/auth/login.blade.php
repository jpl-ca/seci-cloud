@extends('site.layouts.essential')

@section('html')
    <div class="container">
        <div class="row">
            @if(\Session::has('errors'))
            <div class="alert alert-danger text-center">
                E-mail o contraseña incorrecta!
            </div>
            @endif
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
	                    <p class="text-center" id="logo-p-wrapper">
	                        <img class="img" src="{{ asset('images/seci-banner--horizontal.png') }}" alt="" style="width:250px">
	                    </p>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{ url('auth/login') }}">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Matener mi sesión iniciada
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Iniciar Sesión</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop