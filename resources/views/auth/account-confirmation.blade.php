@extends('site.layouts.essential')

@section('html')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
	                    <p class="text-center" id="logo-p-wrapper">
	                        <img class="img" src="{{ asset('images/seci-banner--horizontal.png') }}" alt="" style="width:250px">
	                    </p>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{ url('auth/confirm') }}">
                            <fieldset>
                                <p>Escriba su email para confirmar su cuenta:</p>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                    <input type="hidden" name="token" value="{{$token}}">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Confirmar Cuenta</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop