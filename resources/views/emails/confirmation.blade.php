<p>{{ trans('confirmations.link') }}</p>
<p>
	<a href="{{ url( config('confirmation.confirmation_url_path'), [$token] ) }}">{{ url( config('confirmation.confirmation_url_path'), [$token] ) }}</a>
</p>
