<?php

use Illuminate\Database\Seeder;

class IncidentAlertStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('incident_alert_states')->insert(array(
            array('name' => 'Nuevo', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Notificado', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'En atención', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Finalizado', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Cancelado', 'created_at' => $date, 'updated_at' => $date),
		));
    }
}
