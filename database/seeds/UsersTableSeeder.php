<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('users')->insert(array(
            array(
                'user_type_id' => '1',
                'first_name' => 'Usuario 1',
                'last_name' => 'Prueba 1',
                'id_number' => '11111111',
                'email' => 'usuario1@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => null,
                'lng' => null,
                'created_at' => $date,
                'updated_at' => $date
            ),
            array(
                'user_type_id' => '1',
                'first_name' => 'Usuario 2',
                'last_name' => 'Prueba 2',
                'id_number' => '22222222',
                'email' => 'usuario2@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => null,
                'lng' => null,
                'created_at' => $date,
                'updated_at' => $date
            ),
            array(
                'user_type_id' => '2',
                'first_name' => 'Agente 1',
                'last_name' => 'Prueba 1',
                'id_number' => '33333333',
                'email' => 'agente1@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => -12.086867,
                'lng' => -77.013569,
                'created_at' => $date,
                'updated_at' => $date
            ),
            array(
                'user_type_id' => '2',
                'first_name' => 'Agente 2',
                'last_name' => 'Prueba 2',
                'id_number' => '44444444',
                'email' => 'agente2@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => -12.056437,
                'lng' => -76.988822,
                'created_at' => $date,
                'updated_at' => $date
            ),
            array(
                'user_type_id' => '3',
                'first_name' => 'Operador 1',
                'last_name' => 'Prueba 1',
                'id_number' => '55555555',
                'email' => 'operador1@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => null,
                'lng' => null,
                'created_at' => $date,
                'updated_at' => $date
            ),
			array(
                'user_type_id' => '4',
                'first_name' => 'Administrador 1',
                'last_name' => 'Prueba 1',
                'id_number' => '66666666',
                'email' => 'admin1@yopmail.com',
                'password' => \Hash::make('password'),
                'verified' => true,
                'lat' => null,
                'lng' => null,
                'created_at' => $date,
                'updated_at' => $date
            ),
		));
    }

}
