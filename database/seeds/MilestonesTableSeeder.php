<?php

use Illuminate\Database\Seeder;

class MilestonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('milestones')->insert(array(
            array('name' => 'Comisaría 1', 'lat' => -12.086867, 'lng' => -77.013569, 'milestone_type_id' => '1', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Comisaría 2', 'lat' => -12.056867, 'lng' => -77.003569, 'milestone_type_id' => '1', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Serenazgo 1', 'lat' => -12.042867, 'lng' => -77.010569, 'milestone_type_id' => '1', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Serenazgo 2', 'lat' => -12.032867, 'lng' => -77.014569, 'milestone_type_id' => '1', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Hospital 1', 'lat' => -12.062867, 'lng' => -77.013569, 'milestone_type_id' => '2', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Hospital 2', 'lat' => -12.071857, 'lng' => -77.024569, 'milestone_type_id' => '2', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Clínica 1', 'lat' => -12.032867, 'lng' => -77.036569, 'milestone_type_id' => '2', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'Clínica 2', 'lat' => -12.081867, 'lng' => -77.043569, 'milestone_type_id' => '2', 'address' => 'Av. Avenida Dirección 123', 'created_at' => $date, 'updated_at' => $date),
		));
    }

}
