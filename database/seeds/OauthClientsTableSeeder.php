<?php

use Illuminate\Database\Seeder;
use Daylight\OAuth\Eloquent\OauthClient;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $androidClient = OauthClient::find('seci_android');

        if(is_null($androidClient))
        {
            $androidClient = OauthClient::newClient('seci_android', 'SECI Oficial Android Client', '8lPZnWyF2KCOj148GIdWRzCov54eMa34J1SB0OD9');
        }

        $webClient = OauthClient::find('seci_web');

        if(is_null($webClient))
        {
            $webClient = OauthClient::newClient('seci_web', 'SECI Oficial Web Client', 'Tg42jS354eMa34SA670OD9332ZnWyF2KCOjy3WGI');
        }
    }
}
