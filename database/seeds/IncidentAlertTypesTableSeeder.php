<?php

use Illuminate\Database\Seeder;

class IncidentAlertTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('incident_alert_types')->insert(array(
			array('name' => 'Asalto/Robo - Silencioso', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Robo a mano armada', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Robo de vehículo', 'created_at' => $date, 'updated_at' => $date),
		));
    }
}
