<?php

use Illuminate\Database\Seeder;

class MilestoneTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('milestone_types')->insert(array(
            array('name' => 'Comisarías y Serenazgo', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Hospitales y Clínicas', 'created_at' => $date, 'updated_at' => $date),
		));
    }

}
