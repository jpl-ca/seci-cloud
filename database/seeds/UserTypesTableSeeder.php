<?php

use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('user_types')->insert(array(
			array('name' => 'Ciudadano', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Agente', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Operador Web Monitoreo', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Administrador Web Monitoreo', 'created_at' => $date, 'updated_at' => $date),
		));
    }

}
