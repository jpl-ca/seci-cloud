<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('id_number');
            $table->string('first_name');
            $table->string('last_name');
            $table->decimal('lat', 22, 20);
            $table->decimal('lng', 22, 20);
            $table->string('address');
            $table->integer('agent_id')->unsigned()->nullable();
            $table->integer('incident_alert_type_id')->unsigned();
            $table->integer('incident_alert_state_id')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('incident_alert_type_id')->references('id')->on('incident_alert_types')->onUpdate('cascade');
            $table->foreign('incident_alert_state_id')->references('id')->on('incident_alert_states')->onUpdate('cascade');
            $table->foreign('agent_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_alerts');
    }
}
