<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('multicast_id');
            $table->integer('success');
            $table->integer('failure');
            $table->integer('canonical_ids');
            $table->integer('notified_user_type_id')->unsigned();
            $table->text('notified_users');
            $table->integer('incident_alert_id')->unsigned();
            $table->integer('incident_alert_state_id')->unsigned();
            $table->timestamps();

            $table->foreign('notified_user_type_id')->references('id')->on('user_types')->onDelete('cascade');
            $table->foreign('incident_alert_id')->references('id')->on('incident_alerts')->onDelete('cascade');
            $table->foreign('incident_alert_state_id')->references('id')->on('incident_alert_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_messages');
    }
}
