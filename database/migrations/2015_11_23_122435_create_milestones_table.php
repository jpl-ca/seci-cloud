<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('lat', 22, 20);
            $table->decimal('lng', 22, 20);
            $table->integer('milestone_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('milestone_type_id')->references('id')->on('milestone_types')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milestones');
    }
}
