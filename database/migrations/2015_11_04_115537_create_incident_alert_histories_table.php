<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentAlertHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_alert_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('incident_alert_id')->unsigned();
            $table->integer('incident_alert_state_id')->unsigned();
            $table->timestamps();

            $table->foreign('incident_alert_id')->references('id')->on('incident_alerts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('incident_alert_state_id')->references('id')->on('incident_alert_states')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_alert_histories');
    }
}
