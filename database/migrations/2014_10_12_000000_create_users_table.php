<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type_id')->unsigned()->default(1);
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('id_number')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->boolean('verified')->default(false);
            $table->decimal('lat', 22, 20)->nullable();
            $table->decimal('lng', 22, 20)->nullable();
            $table->string('gcm_id')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_type_id')->references('id')->on('user_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
