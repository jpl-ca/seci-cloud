<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationMessageResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_message_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 10);
            $table->string('result', 40);
            $table->integer('notification_message_id')->unsigned();
            $table->timestamps();

            $table->foreign('notification_message_id')->references('id')->on('notification_messages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_message_results');
    }
}
